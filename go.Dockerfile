FROM golang:1.22-bullseye

# Install OpenSSH server
RUN apt-get update \
    && DEBIAN_FRONTEND=noninteractive apt-get install -y openssh-server \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*
# Create the missing directory for privilege separation
RUN mkdir /run/sshd

# Set the password for the root user (Change 'your_password' to your desired password)
RUN echo 'root:code' | chpasswd

# Configure SSH server to allow root login
RUN sed -i 's/#PermitRootLogin prohibit-password/PermitRootLogin yes/' /etc/ssh/sshd_config

# Expose SSH port
EXPOSE 22

# Start SSH service
CMD ["/usr/sbin/sshd", "-D"]